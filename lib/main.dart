import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Food App',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Welcome to Food Explore'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        children:<Widget>[
          Container(
            height:MediaQuery.of(context).size.height/2,
            width:MediaQuery.of(context).size.width,
            color:Color(0xFFAF7AC5 ),

            child:Column(
              children:<Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 50.0, right:200.0),
                  child:Text(
                    "Food  Blog",
                    style:TextStyle(
                        fontFamily: 'Pacifico',
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0,
                    ),
                  ),
                ),
        Padding(
          padding: EdgeInsets.only(top:20.0),
          child:Container(
              child:Image.asset("assets/Biriyani.jpg"),
              height: 218.0,
              width:250.0,
    ),
        ),
    ],
            ),
          ),
          Expanded(
            child:Container(
              color:Colors.greenAccent,
                child:Stack(
    children: <Widget>[
      Padding(
    padding: ( EdgeInsets.only(top:2.0)),
    child:Column(
    children:[
     Align(
       child:Text('Biryani',
    style:TextStyle(
    fontFamily: 'Pacifico',
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 35.0,
    ),
       ),
     ),
    ]
    ) ,
    ),
      Padding(
        padding: ( EdgeInsets.only(top:80.0)),
        child:Column(
            children:[
              Align(
                alignment: Alignment.center,
                child:Text('Biryani is a mixed rice dish with its '
                    'origins among the Muslims of India.It can be compared to mixing '
                    'a curry, later combining it with semi-cooked rice separately.'
                    'This dish is especially popular throughout the Indian subcontinent, '
                    'as well as among its diaspora. It is also prepared in other regions'
                    ' such as Iraqi Kurdistan.It is made with Indian spices, rice, and meat.'
                    ' It tastes delicious and it is my favorite food.',
                  style:TextStyle(
                    fontFamily: 'Pacifico',
                    color: Colors.black,
                    fontSize: 15.0,
                  ),
                ),
              ),
            ]
        ) ,
      ),
      Padding(
        padding: ( EdgeInsets.only(top:250.0)),
        child:Column(
          children:[
            Align(
              alignment: Alignment.center,
              child:Text('Yummy!!!',
                style:TextStyle(
                  fontFamily: 'Pacifico',
                  color: Colors.pink,
                  fontSize: 30.0,
                ),
              ),
            ),
          ],
        ) ,
      ),
      Padding(
        padding: ( EdgeInsets.only(top:300.0)),
        child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:<Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back_ios),
                color: Colors.black,
              ),
              IconButton(
                icon: Icon(Icons.thumb_up),
                color: Colors.black,
              ),
              IconButton(
                icon: Icon(Icons.restaurant),
                color: Colors.black,
              ),
            ]
        ),
      ),
    ],
        ) ,
      ),
          ),
    ],
    ),
    );
  }
}


